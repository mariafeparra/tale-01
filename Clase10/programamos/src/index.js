var moment = require('moment');
console.log('Horario local' +  moment().format());
console.log('Horario UTC' +  moment.utc().format());
const diferencia = moment.utc().subtract(5, 'hours');
console.log('Diferencia horario es de.:' + diferencia);
console.log(`${moment().hours()} - ${moment.utc()} = ${diferencia.hours}`);