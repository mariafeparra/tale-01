class Persona{
    constructor(nombre,apellido,edad){
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }
    
    getFullName (){
        return this.nombre + "" + this.apellido;

    }

    es_mayor (){
        return this.edad >= 18;
    }

}
let persona1 = new Persona ("Juan", "Puello", 16);
console.log(persona1.getFullName());
console.log(persona1.es_mayor());
